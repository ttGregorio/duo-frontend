import { Partner } from './partner.model';
import { ProductType } from './product-type.model';
import { ProductSubType } from './product-sub-type.model';
import { ProductImage } from './product-image.model';

export class Product{
    constructor(
        public id:string,
        public name:string,
        public description:string,
        public value:number,
        public partner:Partner,
        public productType:ProductType,
        public productSubType:ProductSubType,
        public active:boolean,
        public productImages:ProductImage[]
    ){}
}
