import { Product } from './product.model';
import { ProductType } from './product-type.model';
import { ProductSubType } from './product-sub-type.model';
import { Partner } from './partner.model';
import { User } from './user.model';

export class Order{
    constructor(
        public id:string,
        public date:Date,
        public username:string,
        public user:User,
        public partner:Partner,
        public productType:ProductType,
        public productSubType:ProductSubType,
        public product:Product,
        public observation:string,
        public quantity:number,
        public unityValue:number,
        public totalValue:number,
        public status:string,
        public paymentResponse:string
    ){}
}
