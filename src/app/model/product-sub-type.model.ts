import { ProductType } from './product-type.model';

export class ProductSubType{
    constructor(
        public id:string,
        public name:string,
        public productType:ProductType,
        public active:boolean
    ){}
}