import { Product } from './product.model';

export class ProductImage{
    constructor(
        public id:string,
        public image:string,
        public name:string,
        public product:Product
    ){}
}
