import { Injectable } from '@angular/core';
import { Product } from '../model/product.model';
import { HttpClient } from '@angular/common/http';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }

  createOrUpdate(productType:Product){
    if(productType.id != null && productType.id != ""){
      return this.http.put(`${ENDPOINT_API}/products`, productType);
    }else{
      productType.id = null;
      return this.http.post(`${ENDPOINT_API}/products`, productType);
    }
  }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/products`);
  }

  finByPartnerProductTypeSubType(productTypeId:string, productSubTypeId:string, partnerId:string){
    return this.http.get(`${ENDPOINT_API}/products/${productTypeId}/${productSubTypeId}/${partnerId}/partner`);
  }

  findAll(page:number, count:number, partnerId:string){
    return this.http.get(`${ENDPOINT_API}/products/${page}/${count}/${partnerId}`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/products/${id}/id`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/products/${id}`);
  }
}
