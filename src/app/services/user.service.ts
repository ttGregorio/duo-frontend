import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENDPOINT_API } from './nex2you.api';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  login(user:User){
    return this.http.post(`${ENDPOINT_API}/api/auth`, user);
  }

  createOrUpdate(user:User){
    if(user.id != null && user.id != ""){
      return this.http.post(`${ENDPOINT_API}/users`, user);
    }else{
      user.id = null;
      return this.http.post(`${ENDPOINT_API}/users`, user);
    }
  }

  findAll(page:number, count:number){
    return this.http.get(`${ENDPOINT_API}/users`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/users/${id}`);
  }

  findByUsername(username:string){
    return this.http.get(`${ENDPOINT_API}/users/username/${username}`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/users/${id}`);
  }
}
