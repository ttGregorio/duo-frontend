import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductImage } from '../model/product-image.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class ProductImageService {

  constructor(private http:HttpClient) { }

  createOrUpdate(productImage:ProductImage){
    if(productImage.id != null && productImage.id != ""){
      return this.http.post(`${ENDPOINT_API}/product-images`, productImage);
    }else{
      productImage.id = null;
      return this.http.post(`${ENDPOINT_API}/product-images`, productImage);
    }
  }

  findAllList(productId:string){
    return this.http.get(`${ENDPOINT_API}/product-images/${productId}`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/product-images/${id}/id`);
  }

  findByName(productId:string,name:string){
    return this.http.get(`${ENDPOINT_API}/product-images/${productId}/${name}/image`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/product-images/${id}`);
  }

  deleteByProduct(productId:string){
    return this.http.delete(`${ENDPOINT_API}/product-images/${productId}/product`);
  }
}
