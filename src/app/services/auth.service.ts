import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { UserCredentials } from '../model/user-credentials.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { }

  login(user:UserCredentials){
    return this.http.post(`${ENDPOINT_API}/users/keycloak/token`, user);
  }

  refresh(token:string){
    const header= new HttpHeaders().append('Authorization', token);
    
    return this.http.get(`${ENDPOINT_API}/users/keycloak/refreshtoken`, {headers : header});
  }
}
