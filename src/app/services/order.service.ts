import { Injectable } from '@angular/core';
import { Order } from '../model/order.model';
import { ENDPOINT_API } from './nex2you.api';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http:HttpClient) { }

  createOrUpdate(order:Order){
    if(order.id != null && order.id != ""){
      return this.http.put(`${ENDPOINT_API}/orders`, order);
    }else{
      order.id = null;
      return this.http.post(`${ENDPOINT_API}/orders`, order);
    }
  }

  findAll(page:number, count:number, partnerId:string){
    return this.http.get(`${ENDPOINT_API}/orders/${page}/${count}/${partnerId}`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/orders/${id}/id`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/orders/${id}`);
  }
}
