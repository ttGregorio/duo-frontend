import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Partner } from '../model/partner.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  constructor(private http:HttpClient) { }

  createOrUpdate(partner:Partner){
    if(partner.id != null && partner.id != ""){
      return this.http.put(`${ENDPOINT_API}/partners`, partner);
    }else{
      partner.id = null;
      return this.http.post(`${ENDPOINT_API}/partners`, partner);
    }
  }

  findAll(page:number, count:number){
    return this.http.get(`${ENDPOINT_API}/partners/${page}/${count}`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/partners/${id}`);
  }

  findImageById(id:string){
    return this.http.get(`${ENDPOINT_API}/partners/${id}/image`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/partners/${id}`);
  }
}
