import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SharedService {

  public static instance:SharedService = null;
 // user:User;
  token:string;
  refreshToken;
  showTemplate = new EventEmitter<boolean>();

  constructor() {
    return SharedService.instance = SharedService.instance || this;
  }

  public static getInstance(){
    if(this.instance == null){
      this.instance = new SharedService();
    }

    return this.instance;
  }

  isLoggedIn():boolean{
    if(this.token == undefined){
      return false;
    } else if(this.token == null){
      return false;
    }
    return this.token != "";
  }

}
