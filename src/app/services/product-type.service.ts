import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductType } from '../model/product-type.model';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class ProductTypeService {

  constructor(private http:HttpClient) { }

  createOrUpdate(productType:ProductType){
    if(productType.id != null && productType.id != ""){
      return this.http.put(`${ENDPOINT_API}/product-types`, productType);
    }else{
      productType.id = null;
      return this.http.post(`${ENDPOINT_API}/product-types`, productType);
    }
  }

  findAllList(){
    return this.http.get(`${ENDPOINT_API}/product-types`);
  }

  findAll(page:number, count:number){
    return this.http.get(`${ENDPOINT_API}/product-types/${page}/${count}`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/product-types/${id}`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/product-types/${id}`);
  }
}
