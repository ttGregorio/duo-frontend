import { Injectable } from '@angular/core';
import { ProductSubType } from '../model/product-sub-type.model';
import { HttpClient } from '@angular/common/http';
import { ENDPOINT_API } from './nex2you.api';

@Injectable({
  providedIn: 'root'
})
export class ProductSubTypeService {

  constructor(private http:HttpClient) { }

  createOrUpdate(productSubType:ProductSubType){
    if(productSubType.id != null && productSubType.id != ""){
      return this.http.post(`${ENDPOINT_API}/product-sub-types`, productSubType);
    }else{
      productSubType.id = null;
      return this.http.post(`${ENDPOINT_API}/product-sub-types`, productSubType);
    }
  }

  findAllList(productTypeId:string){
    return this.http.get(`${ENDPOINT_API}/product-sub-types/${productTypeId}`);
  }

  findAll(page:number, count:number, productTypeId:string){
    return this.http.get(`${ENDPOINT_API}/product-sub-types/${page}/${count}/${productTypeId}`);
  }

  findById(id:string){
    return this.http.get(`${ENDPOINT_API}/product-sub-types/${id}/id`);
  }

  delete(id:string){
    return this.http.delete(`${ENDPOINT_API}/product-sub-types/${id}`);
  }
}
