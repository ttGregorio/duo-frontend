import {Routes, RouterModule} from '@angular/router'
import { HomeComponent } from './components/home/home.component'
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from './components/security/auth.guard';
import { ListComponent } from './components/user/list/list.component';
import { CreateUpdateComponent } from './components/user/create-update/create-update.component';
import { PartnerListComponent } from './components/partner/partner-list/partner-list.component';
import { PartnerCreateUpdateComponent } from './components/partner/partner-create-update/partner-create-update.component';
import { ProductTypeListComponent } from './components/product-type/product-type-list/product-type-list.component';
import { ProductTypeCreateUpdateComponent } from './components/product-type/product-type-create-update/product-type-create-update.component';
import { ProductSubTypeListComponent } from './components/product-type/product-sub-type/product-sub-type-list/product-sub-type-list.component';
import { ProductSubTypeCreateUpdateComponent } from './components/product-type/product-sub-type/product-sub-type-create-update/product-sub-type-create-update.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductCreateUpdateComponent } from './components/product/product-create-update/product-create-update.component';
import { OrderListComponent } from './components/order/order-list/order-list.component';
import { OrderCreateUpdateComponent } from './components/order/order-create-update/order-create-update.component';



export const ROUTES: Routes = [
    {path: '', component: HomeComponent,  canActivate: [AuthGuard]},
    {path: 'user-list', component: ListComponent,  canActivate: [AuthGuard]},
    {path: 'user-create-update', component: CreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'user-create-update/:id', component: CreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'partner-list', component: PartnerListComponent,  canActivate: [AuthGuard]},
    {path: 'partner-create-update', component: PartnerCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'partner-create-update/:id', component: PartnerCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'product-type-list', component: ProductTypeListComponent,  canActivate: [AuthGuard]},
    {path: 'product-type-create-update', component: ProductTypeCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'product-type-create-update/:id', component: ProductTypeCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'product-sub-type-list/:id', component: ProductSubTypeListComponent,  canActivate: [AuthGuard]},
    {path: 'product-sub-type-create-update/:id', component: ProductSubTypeCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'product-list', component: ProductListComponent,  canActivate: [AuthGuard]},
    {path: 'product-create-update', component: ProductCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'product-create-update/:id', component: ProductCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'order-list', component: OrderListComponent,  canActivate: [AuthGuard]},
    {path: 'order-create-update', component: OrderCreateUpdateComponent,  canActivate: [AuthGuard]},
    {path: 'order-create-update/:id', component: OrderCreateUpdateComponent,  canActivate: [AuthGuard]}
]


export const routes: ModuleWithProviders = RouterModule.forRoot(ROUTES);
