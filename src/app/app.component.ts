import { SharedService } from './services/shared.service';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  showTemplate:boolean = false;
  public shared:SharedService;

  constructor(public translate: TranslateService){
    this.shared = SharedService.getInstance();
    translate.addLangs(['en', 'es', 'pt']);
    translate.setDefaultLang('en');      
    
  //  const browserLang = translate.getBrowserLang();
  //  translate.use('en');
  }

  ngOnInit(){
    this.shared.showTemplate.subscribe(show => this.showTemplate = show);
  }

  showContentWrapper(){
    return {
//      'content-wrapper':this.shared.isLoggedIn()
        'content-wrapper':true
    }
  }
}
