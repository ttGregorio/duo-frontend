import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, DoBootstrap, ApplicationRef, APP_INITIALIZER } from '@angular/core';
import { ImageCropperModule } from 'ngx-image-cropper';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/security/login/login.component';
import { routes } from './app.routes';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthGuard } from './components/security/auth.guard';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt-PT';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { ListComponent } from './components/user/list/list.component';
import { CreateUpdateComponent } from './components/user/create-update/create-update.component';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { initializer } from './app-init';
import { DialogService } from './services/dialog.service';
import { PartnerListComponent } from './components/partner/partner-list/partner-list.component';
import { PartnerCreateUpdateComponent } from './components/partner/partner-create-update/partner-create-update.component';
import { ProductTypeListComponent } from './components/product-type/product-type-list/product-type-list.component';
import { ProductTypeCreateUpdateComponent } from './components/product-type/product-type-create-update/product-type-create-update.component';
import { ProductSubTypeListComponent } from './components/product-type/product-sub-type/product-sub-type-list/product-sub-type-list.component';
import { ProductSubTypeCreateUpdateComponent } from './components/product-type/product-sub-type/product-sub-type-create-update/product-sub-type-create-update.component';
import { ProductListComponent } from './components/product/product-list/product-list.component';
import { ProductCreateUpdateComponent } from './components/product/product-create-update/product-create-update.component';
import { NgImageSliderModule } from 'ng-image-slider';
import { OrderListComponent } from './components/order/order-list/order-list.component';
import { OrderCreateUpdateComponent } from './components/order/order-create-update/order-create-update.component';
import { UserPreferencesSubListComponent } from './components/user/user-preferences-sub-list/user-preferences-sub-list.component';

registerLocaleData(localePt, 'pt');

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const keycloakService = new KeycloakService();

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    ListComponent,
    CreateUpdateComponent,
    PartnerListComponent,
    PartnerCreateUpdateComponent,
    ProductTypeListComponent,
    ProductTypeCreateUpdateComponent,
    ProductSubTypeListComponent,
    ProductSubTypeCreateUpdateComponent,
    ProductListComponent,
    ProductCreateUpdateComponent,
    OrderListComponent,
    OrderCreateUpdateComponent,
    UserPreferencesSubListComponent
  ],
  imports: [
    NgImageSliderModule,
    ImageCropperModule,
    KeycloakAngularModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    routes,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    },
    {
      provide: KeycloakService,
      useValue: keycloakService
    },
    AuthGuard,
    DialogService,
    {provide: LOCALE_ID, useValue: 'pt-PT' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule implements DoBootstrap {
  ngDoBootstrap(appRef: ApplicationRef) {
    keycloakService
      .init()
      .then(() => {
        console.log('[ngDoBootstrap] bootstrap app');

        appRef.bootstrap(AppComponent);
      })
      .catch(error => console.error('[ngDoBootstrap] init Keycloak failed', error));
  }
}
