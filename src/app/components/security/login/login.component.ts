import { CurrentUser } from './../../../model/current-user.model';
import { UserService } from './../../../services/user.service';
import { User } from './../../../model/user.model';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../services/shared.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserCredentials } from 'src/app/model/user-credentials.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new UserCredentials('','');
  shared:SharedService;
  message:string;

  constructor(  
    private translate: TranslateService,  
    private userService:AuthService,
    private router:Router
  ) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
  }

  login(){
    this.message = '';
    this.userService.login(this.user).subscribe((userAuthentication: CurrentUser) =>{
      console.log(userAuthentication);
      this.shared.token = userAuthentication.access_token;
      this.shared.refreshToken = userAuthentication.refresh_token;
    //  this.shared.user = userAuthentication.user;
   //   this.shared.user.profile = this.shared.user.profile.substring(5);
      if(userAuthentication.refresh_token != undefined){
        this.shared.showTemplate.emit(true);
        this.router.navigate(['/']);  
      }
    }, err => {
      this.shared.token = null;
//      this.shared.user = null;
      this.shared.showTemplate.emit(false);
      this.message = 'Erro';
    });
  }

  changeLanguage(language:string){
    this.translate.use(language);
  }

  cancelLogin(){
    this.message = '';
    this.user = new UserCredentials('','');
    window.location.href = '/login';
    window.location.reload();
  }

  getFromGroupClass(isInvalid:boolean, isDirty:boolean){
    return {
      'form-group': true,
      'has-error': isInvalid && isDirty,
      'has-success': isInvalid && isDirty
    };
  }
}