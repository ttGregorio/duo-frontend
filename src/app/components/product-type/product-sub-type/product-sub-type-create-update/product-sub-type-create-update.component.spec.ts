import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSubTypeCreateUpdateComponent } from './product-sub-type-create-update.component';

describe('ProductSubTypeCreateUpdateComponent', () => {
  let component: ProductSubTypeCreateUpdateComponent;
  let fixture: ComponentFixture<ProductSubTypeCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSubTypeCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSubTypeCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
