import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/app/services/dialog.service';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ProductSubTypeService } from 'src/app/services/product-sub-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseApi } from 'src/app/model/response-api.model';
import { ProductType } from 'src/app/model/product-type.model';
import { ProductSubType } from 'src/app/model/product-sub-type.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-sub-type-create-update',
  templateUrl: './product-sub-type-create-update.component.html',
  styleUrls: ['./product-sub-type-create-update.component.css']
})
export class ProductSubTypeCreateUpdateComponent implements OnInit {

  message = {};

  classCss = {};

  productType:ProductType = new ProductType('','',true);
  productSubType:ProductSubType = new ProductSubType('','',new ProductType('','',true),true);

  constructor(
      private translate: TranslateService,
      private productTypeService: ProductTypeService,
      private productSubTypeService: ProductSubTypeService,
      private route:ActivatedRoute,
      private router: Router
  ) { }

  ngOnInit() {
    let dados:string = this.route.snapshot.params['id'];
    let ids = dados.split("|");
    this.findProductType(ids["0"]);
    if(ids[1] != undefined && ids[1] != ""){
      this.findById(ids[1]);
    }
  }

  register(){
    this.message = {};
    this.productSubType.productType = this.productType;
    this.productSubTypeService.createOrUpdate(this.productSubType).subscribe((responseApi:ResponseApi)=> {
      let productTypeRet:ProductSubType = responseApi.data;
      let dados:string = this.route.snapshot.params['id'];
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      let ids = dados.split("|");
    if(ids != undefined && ids.length > 1 && ids[1] != ""){
        this.translate.get('productSubTypeEdited').subscribe((productTypeEdited: string) => {
          Toast.fire({
            icon: 'success',
            title: productTypeEdited
          })
          this.router.navigate(['/product-sub-type-list', this.productType.id]);
        });
      }else{
        this.translate.get('productSubTypeCreated').subscribe((productTypeCreated: string) => {
          Toast.fire({
            icon: 'success',
            title: productTypeCreated
          })
          this.router.navigate(['/product-sub-type-list', this.productType.id]);
        });
      }
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });   
    });
  }

  findProductType(productTypeId:string) {
    this.productTypeService.findById(productTypeId).subscribe((responseApi: ResponseApi) => {
        this.productType = responseApi.data;
    }, err => {
        this.showMessage({
            type: 'error',
            text: err['error']['errors'][0]
        });
    });
}

findById(id:string){
  this.productSubTypeService.findById(id).subscribe((responseApi: ResponseApi) => {
    this.productSubType = responseApi.data;
  }, err =>{
    this.showMessage({
      type:'error',
      text:err['error']['errors'][0]
    });
  });
}

getFromGroupClass(value:string){
  return {
    'form-group': true,
    'has-error': value == "",
    'has-success': value != ""
  };
}

private showMessage(message: {
  type: string,
  text: string
}): void {
  this.message = message;
  this.buildClasses(message.type);
  setTimeout(() => {
      this.message = undefined
  }, 10000);
}

private buildClasses(type: string): void {
  this.classCss = {
      'alert': true
  }

  this.classCss['alert-' + type] = true;
}

}
