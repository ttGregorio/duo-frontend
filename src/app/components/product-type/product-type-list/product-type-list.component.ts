import { Component, OnInit } from '@angular/core';
import { ProductType } from 'src/app/model/product-type.model';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/app/services/dialog.service';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ResponseApi } from 'src/app/model/response-api.model';

@Component({
  selector: 'app-product-type-list',
  templateUrl: './product-type-list.component.html',
  styleUrls: ['./product-type-list.component.css']
})
export class ProductTypeListComponent implements OnInit {

  page: number = 0;

  count: number = 5;

  pages: Array < number > ;

  message = {};

  classCss = {};

  listProductType:ProductType[] = [];

  constructor(
      private translate: TranslateService,
      private dialog: DialogService,
      private productTypeService: ProductTypeService,
      private router: Router) {}

  ngOnInit() {
      this.findAll(this.page, this.count);
  }

  new() {
      this.router.navigate(['/product-type-create-update']);
  }

  productSubTypes(id: string) {
    this.router.navigate(['/product-sub-type-list', id]);
}

  edit(id: string) {
      this.router.navigate(['/product-type-create-update', id]);
  }

  delete(id: string) {
      const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
      })

      const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
      })

      this.translate.get('areYouSure').subscribe((areYouSure: string) => {
          this.translate.get('youWontDeAbleToRevertThis').subscribe((youWontDeAbleToRevertThis: string) => {
              this.translate.get('yes').subscribe((yes: string) => {
                  this.translate.get('no').subscribe((no: string) => {

                      swalWithBootstrapButtons.fire({
                          title: areYouSure,
                          text: youWontDeAbleToRevertThis,
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonText: yes,
                          cancelButtonText: no,
                          reverseButtons: true
                      }).then((result) => {
                          if (result.value) {
                              this.message = {};
                              this.productTypeService.delete(id).subscribe((responseApi: ResponseApi) => {
                                  this.translate.get('productTypeDeleted').subscribe((productTypeDeleted: string) => {
                                      Toast.fire({
                                          icon: 'success',
                                          title: productTypeDeleted
                                      })
                                  });
                                  this.findAll(this.page, this.count);
                              });
                          } else if (
                              /* Read more about handling dismissals below */
                              result.dismiss === Swal.DismissReason.cancel
                          ) {}
                      });
                  });
              });
          });
      });
  }

  findAll(page: number, count: number) {
      this.productTypeService.findAll(page, count).subscribe((responseApi: ResponseApi) => {
          this.listProductType = responseApi.data.content;
          this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
          this.showMessage({
              type: 'error',
              text: err['error']['errors'][0]
          });
      });
  }

  private showMessage(message: {
      type: string,
      text: string
  }): void {
      this.message = message;
      this.buildClasses(message.type);
      setTimeout(() => {
          this.message = undefined
      }, 10000);
  }

  private buildClasses(type: string): void {
      this.classCss = {
          'alert': true
      }

      this.classCss['alert-' + type] = true;
  }

  setNextPage(event: any) {
      event.preventDefault();
      if (this.page + 1 < this.pages.length) {
          this.page = this.page + 1;
          this.findAll(this.page, this.count);
      }
  }

  setPreviousPage(event: any) {
      event.preventDefault();
      if (this.page - 1 < 0) {
          this.page = this.page - 1;
          this.findAll(this.page, this.count);
      }
  }

  setPage(i: number, event: any) {
      event.preventDefault();
      this.page = i;
      this.findAll(this.page, this.count);
  }

}
