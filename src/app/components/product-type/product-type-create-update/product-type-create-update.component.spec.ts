import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductTypeCreateUpdateComponent } from './product-type-create-update.component';

describe('ProductTypeCreateUpdateComponent', () => {
  let component: ProductTypeCreateUpdateComponent;
  let fixture: ComponentFixture<ProductTypeCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductTypeCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductTypeCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
