import { Component, OnInit } from '@angular/core';
import { ProductType } from 'src/app/model/product-type.model';
import { TranslateService } from '@ngx-translate/core';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseApi } from 'src/app/model/response-api.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-type-create-update',
  templateUrl: './product-type-create-update.component.html',
  styleUrls: ['./product-type-create-update.component.css']
})
export class ProductTypeCreateUpdateComponent implements OnInit {

  productType:ProductType = new ProductType('','',true);
  message:{};
  classCss:{};

  constructor(
    private translate: TranslateService, 
    private productTypeService:ProductTypeService, 
    private route:ActivatedRoute, 
    private router:Router
  ) { }

  ngOnInit() {
    let id:string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findById(id);
    }
  }

  findById(id:string){
    this.productTypeService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.productType = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  register(){
    this.message = {};
    this.productTypeService.createOrUpdate(this.productType).subscribe((responseApi:ResponseApi)=> {
     // this.productType  = new ProductType('','',true);
      let productTypeRet:ProductType = responseApi.data;
      let id:string = this.route.snapshot.params['id'];
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      if(id != undefined){
        this.translate.get('productTypeEdited').subscribe((productTypeEdited: string) => {
          Toast.fire({
            icon: 'success',
            title: productTypeEdited
          })
          this.router.navigate(['/product-type-list']);
        });
      }else{
        this.translate.get('productTypeCreated').subscribe((productTypeCreated: string) => {
          Toast.fire({
            icon: 'success',
            title: productTypeCreated
          })
          this.router.navigate(['/product-type-list']);
        });
      }
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });   
    });
  }

  getFromGroupClass(value:string){
    return {
      'form-group': true,
      'has-error': value == "",
      'has-success': value != ""
    };
  }
  
  private showMessage(message:{type:string, text:string}):void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined
    }, 10000);
  }

  private buildClasses(type:string):void{
    this.classCss = {
      'alert':true
    }

    this.classCss['alert-'+type] = true;
  }


}
