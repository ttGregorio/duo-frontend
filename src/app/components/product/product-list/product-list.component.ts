import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product.model';
import { TranslateService } from '@ngx-translate/core';
import { DialogService } from 'src/app/services/dialog.service';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';
import { ResponseApi } from 'src/app/model/response-api.model';
import { KeycloakService } from 'keycloak-angular';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/model/user.model';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  page: number = 0;

  count: number = 5;

  pages: Array < number > ;

  message = {};

  classCss = {};

  listProducts:Product[] = [];

  user:User;

  constructor(
      private userService:UserService, 
      private keycloakService:KeycloakService,
      private translate: TranslateService,
      private productService: ProductService,
      private router: Router) {}

  async ngOnInit() {
      var username:string = await this.keycloakService.getUsername();

      this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
        this.user = responseApi.data;
        this.findAll(this.page, this.count, this.user.partnerId);
      }, err =>{
       /* this.showMessage({
          type:'error',
          text:err['error']['errors'][0]
        });*/
      });
  }

  new() {
      this.router.navigate(['/product-create-update']);
  }

  edit(id: string) {
      this.router.navigate(['/product-create-update', id]);
  }

  delete(id: string) {
      const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
      })

      const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
      })

      this.translate.get('areYouSure').subscribe((areYouSure: string) => {
          this.translate.get('youWontDeAbleToRevertThis').subscribe((youWontDeAbleToRevertThis: string) => {
              this.translate.get('yes').subscribe((yes: string) => {
                  this.translate.get('no').subscribe((no: string) => {

                      swalWithBootstrapButtons.fire({
                          title: areYouSure,
                          text: youWontDeAbleToRevertThis,
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonText: yes,
                          cancelButtonText: no,
                          reverseButtons: true
                      }).then((result) => {
                          if (result.value) {
                              this.message = {};
                              this.productService.delete(id).subscribe((responseApi: ResponseApi) => {
                                  this.translate.get('productTypeDeleted').subscribe((productTypeDeleted: string) => {
                                      Toast.fire({
                                          icon: 'success',
                                          title: productTypeDeleted
                                      })
                                  });
                                  this.findAll(this.page, this.count, this.user.partnerId);
                                });
                          } else if (
                              /* Read more about handling dismissals below */
                              result.dismiss === Swal.DismissReason.cancel
                          ) {}
                      });
                  });
              });
          });
      });
  }

  findAll(page: number, count: number, partnerId:string) {
      this.productService.findAll(page, count, partnerId).subscribe((responseApi: ResponseApi) => {
          this.listProducts = responseApi.data.content;
          this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
          this.showMessage({
              type: 'error',
              text: err['error']['errors'][0]
          });
      });
  }

  private showMessage(message: {
      type: string,
      text: string
  }): void {
      this.message = message;
      this.buildClasses(message.type);
      setTimeout(() => {
          this.message = undefined
      }, 10000);
  }

  private buildClasses(type: string): void {
      this.classCss = {
          'alert': true
      }

      this.classCss['alert-' + type] = true;
  }

  setNextPage(event: any) {
      event.preventDefault();
      if (this.page + 1 < this.pages.length) {
          this.page = this.page + 1;
          this.findAll(this.page, this.count, this.user.partnerId);
        }
  }

  setPreviousPage(event: any) {
      event.preventDefault();
      if (this.page - 1 < 0) {
          this.page = this.page - 1;
          this.findAll(this.page, this.count, this.user.partnerId);
        }
  }

  setPage(i: number, event: any) {
      event.preventDefault();
      this.page = i;
      this.findAll(this.page, this.count, this.user.partnerId);
    }


}
