import { Component, OnInit, ViewChild } from '@angular/core';
import { ResponseApi } from 'src/app/model/response-api.model';
import Swal from 'sweetalert2';
import { ProductType } from 'src/app/model/product-type.model';
import { TranslateService } from '@ngx-translate/core';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/model/product.model';
import { Partner } from 'src/app/model/partner.model';
import { ProductSubType } from 'src/app/model/product-sub-type.model';
import { ProductService } from 'src/app/services/product.service';
import { ProductSubTypeService } from 'src/app/services/product-sub-type.service';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';
import { KeycloakService } from 'keycloak-angular';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ProductImageService } from 'src/app/services/product-image.service';
import { ProductImage } from 'src/app/model/product-image.model';

@Component({
  selector: 'app-product-create-update',
  templateUrl: './product-create-update.component.html',
  styleUrls: ['./product-create-update.component.css']
})
export class ProductCreateUpdateComponent implements OnInit {

  productTypesList:ProductType[] = [];

  productSubTypesList:ProductSubType[] = [];

  productImagesList:ProductImage[] = [];

  currentImage;

  imageObject: Array<object> = [];

  imageChangedEvent: any = '';

  product:Product = new Product('','', '',0,new Partner('','','',true),
                                new ProductType('','',true), new ProductSubType('','',
                                new ProductType('','',true),true),true,[]);
  message:{};
  classCss:{};

  user:User;


  constructor(
    private translate: TranslateService,
    private productImageService:ProductImageService,
    private userService:UserService,
    private keycloakService:KeycloakService,
    private productTypeService:ProductTypeService,
    private productSubTypeService:ProductSubTypeService,
    private productService:ProductService,
    private route:ActivatedRoute,
    private router:Router
  ) { }

  async ngOnInit() {
    var username:string = await this.keycloakService.getUsername();

    this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
      this.user = responseApi.data;
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });

    let id:string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findById(id);
    }
    this.findProductTypes();
  }


  loadProductSubTypes(){
    this.productSubTypeService.findAllList(this.product.productType.id).subscribe((responseApi: ResponseApi) => {
      this.productSubTypesList = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  findProductTypes(){
    this.productTypeService.findAllList().subscribe((responseApi: ResponseApi) => {
      this.productTypesList = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  findById(id:string){
    this.productService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.product = responseApi.data;

      for(var i:number = 0; i < this.product.productImages.length; i++){
        this.productImageService.findByName(this.product.id, this.product.productImages[i].name).subscribe((resq: ResponseApi ) =>{
          this.imageObject.push({
            image: resq.data ,
            thumbImage: resq.data,
            title: '',
            alt: ''
          });
        });
      }

      this.loadProductSubTypes();
    }, err =>{
      console.log(err);
    });
  }

  addImage(){
    if(this.currentImage != undefined && this.currentImage != ""){
      this.productImagesList.push(new ProductImage(null,this.currentImage, null,this.product));
      this.imageObject.push({
        image: this.currentImage,
        thumbImage: this.currentImage,
        title: '',
        alt: ''
      });

      this.currentImage = "";
    }
  }

  register(){
    this.message = {};

    this.productImageService.deleteByProduct(this.user.partnerId).subscribe((responseApi: ResponseApi) => {
    });

    this.product.partner.id = this.user.partnerId;
    this.productService.createOrUpdate(this.product).subscribe((responseApi:ResponseApi)=> {
      let productRet:Product = responseApi.data;

      for(var i:number = 0; i < this.productImagesList.length; i++){
        this.productImagesList[i].product = productRet;
        this.productImageService.createOrUpdate(this.productImagesList[i]).subscribe((responseApi:ResponseApi)=> {
        }, err =>{
        this.showMessage({
          type:'error',
          text:err['error']['errors'][0]
        });
      });
      }

      let id:string = this.route.snapshot.params['id'];
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      if(id != undefined){
        this.translate.get('productEdited').subscribe((productEdited: string) => {
          Toast.fire({
            icon: 'success',
            title: productEdited
          })
          this.router.navigate(['/product-list']);
        });
      }else{
        this.translate.get('productCreated').subscribe((productCreated: string) => {
          Toast.fire({
            icon: 'success',
            title: productCreated
          })
          this.router.navigate(['/product-list']);
        });
      }
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  getFromGroupClass(value:string){
    return {
      'form-group': true,
      'has-error': value == "",
      'has-success': value != ""
    };
  }

  private showMessage(message:{type:string, text:string}):void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined
    }, 10000);
  }

  private buildClasses(type:string):void{
    this.classCss = {
      'alert':true
    }

    this.classCss['alert-'+type] = true;
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.currentImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
}
