import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';
import { ResponseApi } from 'src/app/model/response-api.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-update',
  templateUrl: './create-update.component.html',
  styleUrls: ['./create-update.component.css']
})
export class CreateUpdateComponent implements OnInit {
  user:User = new User('','','','','','', 'pt', '');
  message:{};
  classCss:{};

  constructor(
    private translate: TranslateService, 
    private userService:UserService, 
    private route:ActivatedRoute
  ) { }

  ngOnInit() {
    let id:string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findByUsername(id);
    }
  }

  findByUsername(username:string){
    this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
      this.user = responseApi.data;
      this.user.password = "";
      console.log(this.user);
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  register(){
    this.message = {};
  //  this.user.profile = 'ROLE_COMPANY';
    this.userService.createOrUpdate(this.user).subscribe((responseApi:ResponseApi)=> {
      this.user  = new User('','','','','','', '', '');
      let userRet:User = responseApi.data;
      let id:string = this.route.snapshot.params['id'];
      if(id != undefined){
        this.translate.get('userEdited').subscribe((res2: string) => {
          this.showMessage({type: 'success', text: res2 });
        });
      }else{
        this.translate.get('userCreated').subscribe((res2: string) => {
          this.showMessage({type: 'success', text: res2 });
        });
      }
 //     this.form.resetForm();
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });   
    });
  }

  getFromGroupClass(value:string){
    return {
      'form-group': true,
      'has-error': value == "",
      'has-success': value != ""
    };
  }
  
  private showMessage(message:{type:string, text:string}):void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined
    }, 10000);
  }

  private buildClasses(type:string):void{
    this.classCss = {
      'alert':true
    }

    this.classCss['alert-'+type] = true;
  }
}
