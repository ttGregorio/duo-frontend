import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPreferencesSubListComponent } from './user-preferences-sub-list.component';

describe('UserPreferencesSubListComponent', () => {
  let component: UserPreferencesSubListComponent;
  let fixture: ComponentFixture<UserPreferencesSubListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPreferencesSubListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPreferencesSubListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
