import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { ResponseApi } from 'src/app/model/response-api.model';
import { DialogService } from 'src/app/services/dialog.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  page:number =0;

  count:number=5;

  pages:Array<number>;

  message = {};

  classCss = {};

  listUser=[];

  constructor(
    private translate: TranslateService, 
    private dialog:DialogService, 
    private userService:UserService, 
    private router:Router) {
   }

  ngOnInit() {
    this.findAll(this.page, this.count);
  }

  new(){
    this.router.navigate(['/user-create-update']);
  }

  edit(id:string){
    this.router.navigate(['/user-create-update', id]);
  }

  delete(id:string){
    this.dialog.confirm('Do you want to delete the user?').then((canDelete:boolean) => {
      if(canDelete){
        this.message = {};
        this.userService.delete(id).subscribe((responseApi:ResponseApi) =>{
          this.showMessage({type: 'success', text:`User deleted` });
          this.findAll(this.page, this.count);  
        });
      }
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });   
    });
  }

  findAll(page:number, count:number){
    this.userService.findAll(page, count).subscribe((responseApi:ResponseApi) => {
      console.log(responseApi);
      this.listUser = responseApi.data;
//      this.pages = new Array(responseApi['data']['totalPages']);
    }, err => {
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });   
    });
  }

  private showMessage(message:{type:string, text:string}):void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined
    }, 10000);
  }

  private buildClasses(type:string):void{
    this.classCss = {
      'alert':true
    }

    this.classCss['alert-type'+type] = true;
  }

  setNextPage(event:any){
    event.preventDefault();
    if(this.page + 1 < this.pages.length){
      this.page = this.page + 1;
      this.findAll(this.page, this.count);
    }
  }

  setPreviousPage(event:any){
    event.preventDefault();
    if(this.page - 1 < 0){
      this.page = this.page - 1;
      this.findAll(this.page, this.count);
    }
  }

  setPage(i:number, event:any){
    event.preventDefault();
    this.page = i;
    this.findAll(this.page, this.count);
  }

}
