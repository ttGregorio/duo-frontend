import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import { KeycloakService } from 'keycloak-angular';
import { ResponseApi } from 'src/app/model/response-api.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private keycloakService:KeycloakService, private userService:UserService, 
    private translate: TranslateService) { }

  async ngOnInit() {
    var username:string = await this.keycloakService.getUsername();

    
    this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
      this.translate.use(responseApi.data.language);
    }, err =>{
     /* this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });*/
    });
  }

  changeLanguage(language:string){
    this.translate.use(language);
  }
}
