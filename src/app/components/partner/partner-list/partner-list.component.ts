import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PartnerService } from 'src/app/services/partner.service';
import { Router } from '@angular/router';
import { DialogService } from 'src/app/services/dialog.service';
import { ResponseApi } from 'src/app/model/response-api.model';
import Swal from 'sweetalert2'
import { Partner } from 'src/app/model/partner.model';

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.css']
})
export class PartnerListComponent implements OnInit {

  page: number = 0;

  count: number = 5;

  pages: Array < number > ;

  message = {};

  classCss = {};

  listPartner:Partner[] = [];

  constructor(
      private translate: TranslateService,
      private dialog: DialogService,
      private partnerService: PartnerService,
      private router: Router) {}

  ngOnInit() {
      this.findAll(this.page, this.count);
  }

  new() {
      this.router.navigate(['/partner-create-update']);
  }

  edit(id: string) {
      this.router.navigate(['/partner-create-update', id]);
  }

  delete(id: string) {
      const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
      })

      const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
      })

      this.translate.get('areYouSure').subscribe((areYouSure: string) => {
          this.translate.get('youWontDeAbleToRevertThis').subscribe((youWontDeAbleToRevertThis: string) => {
              this.translate.get('yes').subscribe((yes: string) => {
                  this.translate.get('no').subscribe((no: string) => {

                      swalWithBootstrapButtons.fire({
                          title: areYouSure,
                          text: youWontDeAbleToRevertThis,
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonText: yes,
                          cancelButtonText: no,
                          reverseButtons: true
                      }).then((result) => {
                          if (result.value) {
                              this.message = {};
                              this.partnerService.delete(id).subscribe((responseApi: ResponseApi) => {
                                  this.translate.get('partnerDeleted').subscribe((partnerDeleted: string) => {
                                      Toast.fire({
                                          icon: 'success',
                                          title: partnerDeleted
                                      })
                                  });
                                  this.findAll(this.page, this.count);
                              });
                          } else if (
                              /* Read more about handling dismissals below */
                              result.dismiss === Swal.DismissReason.cancel
                          ) {}
                      });
                  });
              });
          });
      });
  }

  findAll(page: number, count: number) {
      this.partnerService.findAll(page, count).subscribe((responseApi: ResponseApi) => {
          this.listPartner = responseApi.data.content;
          this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
          this.showMessage({
              type: 'error',
              text: err['error']['errors'][0]
          });
      });
  }

  private showMessage(message: {
      type: string,
      text: string
  }): void {
      this.message = message;
      this.buildClasses(message.type);
      setTimeout(() => {
          this.message = undefined
      }, 10000);
  }

  private buildClasses(type: string): void {
      this.classCss = {
          'alert': true
      }

      this.classCss['alert-' + type] = true;
  }

  setNextPage(event: any) {
      event.preventDefault();
      if (this.page + 1 < this.pages.length) {
          this.page = this.page + 1;
          this.findAll(this.page, this.count);
      }
  }

  setPreviousPage(event: any) {
      event.preventDefault();
      if (this.page - 1 < 0) {
          this.page = this.page - 1;
          this.findAll(this.page, this.count);
      }
  }

  setPage(i: number, event: any) {
      event.preventDefault();
      this.page = i;
      this.findAll(this.page, this.count);
  }

}