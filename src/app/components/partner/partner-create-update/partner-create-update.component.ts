import { Component, OnInit } from '@angular/core';
import { ResponseApi } from 'src/app/model/response-api.model';
import { Partner } from 'src/app/model/partner.model';
import { PartnerService } from 'src/app/services/partner.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2'
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-partner-create-update',
  templateUrl: './partner-create-update.component.html',
  styleUrls: ['./partner-create-update.component.css']
})
export class PartnerCreateUpdateComponent implements OnInit {

  partner:Partner = new Partner('','', '',true);
  message:{};
  classCss:{};
  imageChangedEvent: any = '';

  partnerLogo:string;

  constructor(
    private translate: TranslateService,
    private partnerService:PartnerService,
    private route:ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit() {
    let id:string = this.route.snapshot.params['id'];
    if(id != undefined){
      this.findById(id);
    }
  }

  findById(id:string){
    this.partnerService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.partner = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });

    this.partnerService.findImageById(id).subscribe((responseApi: ResponseApi) => {
      this.partnerLogo = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  register(){
    this.message = {};
    this.partnerService.createOrUpdate(this.partner).subscribe((responseApi:ResponseApi)=> {
      this.partner  = new Partner('','', '',true);
      let partnerRet:Partner = responseApi.data;
      let id:string = this.route.snapshot.params['id'];
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      if(id != undefined){
        this.translate.get('partnerEdited').subscribe((partnerEdited: string) => {
          Toast.fire({
            icon: 'success',
            title: partnerEdited
          })
          this.router.navigate(['/partner-list']);
        });
      }else{
        this.translate.get('partnerCreated').subscribe((partnerCreated: string) => {
          Toast.fire({
            icon: 'success',
            title: partnerCreated
          })
          this.router.navigate(['/partner-list']);
        });
      }
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  getFromGroupClass(value:string){
    return {
      'form-group': true,
      'has-error': value == "",
      'has-success': value != ""
    };
  }

  private showMessage(message:{type:string, text:string}):void{
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined
    }, 10000);
  }

  private buildClasses(type:string):void{
    this.classCss = {
      'alert':true
    }

    this.classCss['alert-'+type] = true;
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.partner.logo = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
}
