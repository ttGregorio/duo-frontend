import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerCreateUpdateComponent } from './partner-create-update.component';

describe('PartnerCreateUpdateComponent', () => {
  let component: PartnerCreateUpdateComponent;
  let fixture: ComponentFixture<PartnerCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartnerCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
