import { Component, OnInit } from '@angular/core';
import { ProductType } from 'src/app/model/product-type.model';
import { ProductSubType } from 'src/app/model/product-sub-type.model';
import { Product } from 'src/app/model/product.model';
import { Partner } from 'src/app/model/partner.model';
import { User } from 'src/app/model/user.model';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import { KeycloakService } from 'keycloak-angular';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseApi } from 'src/app/model/response-api.model';
import Swal from 'sweetalert2';
import { Order } from 'src/app/model/order.model';
import { OrderService } from 'src/app/services/order.service';
import { ProductSubTypeService } from 'src/app/services/product-sub-type.service';
import { ProductTypeService } from 'src/app/services/product-type.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-order-create-update',
  templateUrl: './order-create-update.component.html',
  styleUrls: ['./order-create-update.component.css']
})
export class OrderCreateUpdateComponent implements OnInit {
  productTypesList:ProductType[] = [];

  productSubTypesList:ProductSubType[] = [];

  productsList:Product[] = [];

  order:Order = new Order(null,null,null,null,new Partner('','','',true),
    new ProductType('','',true),new ProductSubType('','',null,true),
    new Product('','','',0,null,null,null,true,[]),'',0,0,0,'OPEN','');

  user:User;

  username:string;

  constructor(
    private userService:UserService,
    private keycloakService:KeycloakService,
    private translate: TranslateService,
    private orderService: OrderService,
    private productSubTypeService:ProductSubTypeService,
    private productTypeService:ProductTypeService,
    private productService:ProductService,
    private route:ActivatedRoute,
    private router:Router
  ) { }

  async ngOnInit() {
    this.findProductTypes();
    this.username = await this.keycloakService.getUsername();

    this.userService.findByUsername(this.username).subscribe((responseApi: ResponseApi) => {
      this.user = responseApi.data;
    });

    if(this.route.snapshot.params['id'] != null){
      this.findById(this.route.snapshot.params['id']);
    }
  }

  findById(id:string){
    this.orderService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.order = responseApi.data;
      this.loadProductSubTypes();
 //     this.loadProducts();
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }


  loadProducts(){
    this.productService.finByPartnerProductTypeSubType(this.order.productType.id, this.order.productSubType.id, this.user.partnerId).subscribe((responseApi: ResponseApi) => {
      this.productsList = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }


  loadProductSubTypes(){
    this.productSubTypeService.findAllList(this.order.productType.id).subscribe((responseApi: ResponseApi) => {
      console.log(responseApi);
      this.productSubTypesList = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  findProductTypes(){
    this.productTypeService.findAllList().subscribe((responseApi: ResponseApi) => {
      this.productTypesList = responseApi.data;
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  loadProductValue(){
    for(var i:number = 0; i < this.productsList.length; i++){
      if(this.productsList[i].id == this.order.product.id){
        this.order.product = this.productsList[i];
      }
    }

    this.order.unityValue = this.order.product.value;
    this.order.quantity = 1;
    this.order.totalValue = this.order.product.value;
  }

  reloadTotalValue(){
    this.order.totalValue = this.order.quantity * this.order.product.value;
  }

  register(){
    this.order.partner.id = this.user.partnerId;
    this.order.username = this.username;

    this.orderService.createOrUpdate(this.order).subscribe((responseApi:ResponseApi)=> {
      let id:string = this.route.snapshot.params['id'];
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })

      if(id != undefined){
        this.translate.get('orderEdited').subscribe((orderEdited: string) => {
          Toast.fire({
            icon: 'success',
            title: orderEdited
          })
          this.router.navigate(['/order-list']);
        });
      }else{
        this.translate.get('orderCreated').subscribe((orderCreated: string) => {
          Toast.fire({
            icon: 'success',
            title: orderCreated
          })
          this.router.navigate(['/order-list']);
        });
      }
    }, err =>{
      this.showMessage({
        type:'error',
        text:err['error']['errors'][0]
      });
    });
  }

  getFromGroupClass(value:string){
    return {
      'form-group': true,
      'has-error': value == "",
      'has-success': value != ""
    };
  }

  private showMessage(message:{type:string, text:string}):void{

  }
}
