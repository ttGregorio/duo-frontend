import { Component, OnInit } from '@angular/core';
import { ResponseApi } from 'src/app/model/response-api.model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { UserService } from 'src/app/services/user.service';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/model/user.model';
import { Order } from 'src/app/model/order.model';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  page: number = 0;

  count: number = 5;

  pages: Array < number > ;

  message = {};

  classCss = {};

  listOrders:Order[] = [];

  user:User;

  constructor(
      private userService:UserService,
      private keycloakService:KeycloakService,
      private translate: TranslateService,
      private orderItemService: OrderService,
      private router: Router) {}

  async ngOnInit() {
      var username:string = await this.keycloakService.getUsername();

      this.userService.findByUsername(username).subscribe((responseApi: ResponseApi) => {
        this.user = responseApi.data;
        this.findAll(this.page, this.count, this.user.partnerId);
      }, err =>{
       /* this.showMessage({
          type:'error',
          text:err['error']['errors'][0]
        });*/
      });
  }

  new() {
      this.router.navigate(['/order-create-update']);
  }

  edit(id: string) {
      this.router.navigate(['/order-create-update', id]);
  }

  findAll(page: number, count: number, partnerId:string) {
      this.orderItemService.findAll(page, count, partnerId).subscribe((responseApi: ResponseApi) => {
          console.log(responseApi);
          this.listOrders = responseApi.data.content;
          this.pages = new Array(responseApi['data']['totalPages']);
      }, err => {
          this.showMessage({
              type: 'error',
              text: err['error']['errors'][0]
          });
      });
  }


  delete(id: string) {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    this.translate.get('areYouSure').subscribe((areYouSure: string) => {
        this.translate.get('youWontDeAbleToRevertThis').subscribe((youWontDeAbleToRevertThis: string) => {
            this.translate.get('yes').subscribe((yes: string) => {
                this.translate.get('no').subscribe((no: string) => {

                    swalWithBootstrapButtons.fire({
                        title: areYouSure,
                        text: youWontDeAbleToRevertThis,
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: yes,
                        cancelButtonText: no,
                        reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            this.message = {};
                            this.orderItemService.delete(id).subscribe((responseApi: ResponseApi) => {
                                this.translate.get('orderCanceled').subscribe((productTypeDeleted: string) => {
                                    Toast.fire({
                                        icon: 'success',
                                        title: productTypeDeleted
                                    })
                                });
                                this.findAll(this.page, this.count, this.user.partnerId);
                            });
                        } else if (
                            /* Read more about handling dismissals below */
                            result.dismiss === Swal.DismissReason.cancel
                        ) {}
                    });
                });
            });
        });
    });
}

  private showMessage(message: {
      type: string,
      text: string
  }): void {
      this.message = message;
      this.buildClasses(message.type);
      setTimeout(() => {
          this.message = undefined
      }, 10000);
  }

  private buildClasses(type: string): void {
      this.classCss = {
          'alert': true
      }

      this.classCss['alert-' + type] = true;
  }

  setNextPage(event: any) {
      event.preventDefault();
      if (this.page + 1 < this.pages.length) {
          this.page = this.page + 1;
          this.findAll(this.page, this.count, this.user.partnerId);
        }
  }

  setPreviousPage(event: any) {
      event.preventDefault();
      if (this.page - 1 < 0) {
          this.page = this.page - 1;
          this.findAll(this.page, this.count, this.user.partnerId);
        }
  }

  setPage(i: number, event: any) {
      event.preventDefault();
      this.page = i;
      this.findAll(this.page, this.count, this.user.partnerId);
    }

}
